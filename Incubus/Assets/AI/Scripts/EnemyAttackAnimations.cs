﻿using UnityEngine;
using System.Collections;

public class EnemyAttackAnimations : MonoBehaviour {


   
    private Transform myTransform;
    private Transform p_Transform;
    private NavMeshAgent myAgent;
    private Navfollow myFollow;
    private Animator myAnimator;
    public float distanceToAttack = 5;
    public float distanceToShoot = 10;

    public float pAttackCD;
    public float sAttackCD;

    public bool pAttackReady;
    public bool sAttackReady;

    private bool anHero;

    private bool attack;
    private bool backOff;

    private bool shootingAim;
	// Use this for initialization
	void Start () {

        myTransform = transform;

        p_Transform = GameObject.FindGameObjectWithTag("Player").transform;

        myAgent = transform.parent.GetComponent<NavMeshAgent>();
        myFollow = transform.parent.GetComponent<Navfollow>();

        myAnimator = transform.GetComponent<Animator>();

        if(Random.Range(1, 4) == 2)
        {
            anHero = true;
        }
        else
        {
            anHero = false;
        }
	
	}
	
	// Update is called once per frame
	void Update () {

        if(pAttackCD <=0)
        {
            pAttackReady = true;
        }
        else
        {
            pAttackReady = false;
            pAttackCD -= Time.deltaTime;
        }

        if(sAttackCD <= 0)
        {
            sAttackReady = true;
        }
        else
        {
            sAttackReady = false;
            sAttackCD -= Time.deltaTime;
        }

        if(anHero)
        {
            if (Vector3.Distance(myTransform.position, p_Transform.position) < distanceToAttack && pAttackReady)
            {
                Attack();
            }
        }
        else
        {
            if (Vector3.Distance(myTransform.position, p_Transform.position) < distanceToShoot && sAttackReady)
            {
                Shoot();
            }
        }

        if(shootingAim)
        {
            transform.parent.rotation = Quaternion.Slerp(transform.parent.rotation, Quaternion.LookRotation(new Vector3(p_Transform.position.x, transform.parent.position.y, p_Transform.position.z) - transform.parent.position), (3 * Time.deltaTime));
        }

        if (Vector3.Distance(myTransform.position, p_Transform.position) > 10 && backOff)
        {
            Chase();
            backOff = false;
        }
    }

    void Attack()
    {
        myAgent.stoppingDistance = 3;
        pAttackCD = 3;
        myAnimator.Play("CyclopsAttack");
        if (Random.Range(1, 4) == 4)
        {
            anHero = false;
        }
    }

    void Shoot()
    {
        myAgent.stoppingDistance = 10;
        sAttackCD = 10;
        myFollow.chasing = false;
        myAnimator.Play("CyclopsShoot");
        myAgent.SetDestination(transform.position);
        
        
        if (Random.Range(1, 4) == 4)
        {
            anHero = true;
        }  
    }
    
    void BackOff()
    {
        backOff = true;

        myFollow.chasing = false;
        myAnimator.Play("Run");
        myAgent.SetDestination(p_Transform.position + new Vector3(Random.Range(-5.0f, 5.0f), 0, Random.Range(-5.0f, 5.0f)));
        if(Vector3.Distance(myTransform.position, p_Transform.position) > 10)
        {
            Debug.Log("Chasing");
            Chase();
        }
    }

    void Chase()
    {
        myFollow.chasing = true;
        myAnimator.Play("Run");
    }

    void LaserEnabled()
    {
        transform.FindChild("Pelvis").FindChild("Spine").FindChild("Head").FindChild("Laser").gameObject.SetActive(true);
        shootingAim = true;

    }

    void LaserDisabled()
    {
        transform.FindChild("Pelvis").FindChild("Spine").FindChild("Head").FindChild("Laser").gameObject.SetActive(false);
        shootingAim = false;
    }
}
