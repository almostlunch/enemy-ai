﻿using UnityEngine;
using System.Collections;

public class Navfollow : MonoBehaviour {

    //Navmesh agent follow's the player

    private NavMeshAgent agent;
    private Transform pTarget;
    public bool chasing = true;
    // Use this for initialization
    void Awake () {
        agent = GetComponent<NavMeshAgent>();
        pTarget = GameObject.FindGameObjectWithTag("Player").transform;
        if(this.gameObject.name == "Imp")
        {
            agent.baseOffset = Random.Range(1.0f, 3.0f);
            agent.radius = Random.Range(0.1f, 2.0f);
        }
        

    }
	
	// Update is called once per frame
	void Update () {

       if(chasing)
        {
            if(this.gameObject.name == "Imp")
            {
                agent.SetDestination(new Vector3(pTarget.position.x + Random.Range(-10.0f, 10.0f), pTarget.position.y, pTarget.position.z + Random.Range(-10.0f, 10.0f)));
            }
            else
            {
                agent.SetDestination(pTarget.position);
                
            }

            transform.LookAt(new Vector3(pTarget.position.x, transform.position.y, pTarget.position.z));

        }




    }
}
