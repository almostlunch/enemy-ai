﻿using UnityEngine;
using System.Collections;

public class Fireball : MonoBehaviour {

    public int maxRange = 10;
    public Transform target;
    public GameObject projectile;
    private Transform m_transform;

    private Transform positionToShoot;

    // Use this for initialization
    void Start()
    {
        GameObject go = GameObject.FindGameObjectWithTag("Player");
        target = go.transform;
        m_transform = GetComponent<Transform>();
        positionToShoot = transform.FindChild("shoot").transform;
    }


    /*public void setProjectile(GameObject obj)
    {
        projectilePrefab = obj;
    }*/

    void Shoot()
    {
        Instantiate(projectile, positionToShoot.position, positionToShoot.rotation);
    }
}
