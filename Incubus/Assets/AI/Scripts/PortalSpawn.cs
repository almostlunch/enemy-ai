﻿using UnityEngine;
using System.Collections;

public class PortalSpawn : MonoBehaviour {

    private Transform myTransform;

    private Animator myAnimator;

    void Start () {
        myTransform = transform;
        myAnimator = transform.GetComponent<Animator>();
    }
	
	public void playAnimation()
    {
        myAnimator.Play("Portal");
    }
    
}
