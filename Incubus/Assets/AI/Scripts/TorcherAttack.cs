﻿using UnityEngine;
using System.Collections;

public class TorcherAttack : MonoBehaviour {

    private Transform myTransform;
    private Transform p_Transform;
    private Navfollow myNavFollow;
    private NavMeshAgent myNavAgent;
    private Vector3 chargeTarget;
    private Vector3 chargedFrom;

    public bool hasHit = false;
    public float chargeSpeed;
    public float chargeAcceleration;

    public float normalSpeed;
    public float normalAcceleration;

    private Animator myAnimator;
    public float distanceToAttack = 20;

    bool targetChosen;
    bool startedCharging;
    bool gotChargeTarget;

    public int pAttackDamage = 5;
    public float pAttacKCD;

    float waitTimer;
    bool waitTimerBool;
    //behaviour
    bool b_PrimaryAttack;
    bool b_BackOff;
    // Use this for initialization
    void Start()
    {

        myTransform = transform;

        p_Transform = GameObject.FindGameObjectWithTag("Player").transform;

        myNavFollow = transform.GetComponent<Navfollow>();

        myNavAgent = transform.GetComponent<NavMeshAgent>();

        myAnimator = transform.GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        //cooldowns
        if (pAttacKCD > 0)
        {
            pAttacKCD -= Time.deltaTime;
        }

        if(waitTimer > 0)
        {
            waitTimer -= Time.deltaTime;
            waitTimerBool = true;
        }

        if(b_PrimaryAttack)
        {
            Charge();
        }
        else if (b_BackOff)
        {
            BackOff();
        }
        else
        {
            Chase();
        }
        

        if (Vector3.Distance(myTransform.position, p_Transform.position) < distanceToAttack && !b_BackOff && pAttacKCD <= 0)
        {            
            if(!startedCharging)
            {
                startedCharging = true;
            }
                
            b_PrimaryAttack = true;           
        }
    }

    void BackOff()
    {
        hasHit = false;
        myNavAgent.stoppingDistance = 10;
        if(myNavAgent)
        if (myNavAgent.speed != normalSpeed)
            myNavAgent.speed = normalSpeed;
        if (myNavAgent.acceleration != normalAcceleration)
            myNavAgent.acceleration = normalAcceleration;

        
        chargedFrom = new Vector3(p_Transform.position.x + Random.Range(-5, 5), myTransform.position.y, p_Transform.position.z + Random.Range(-5, 5));
        myAnimator.Play("ChildRun");
        myNavAgent.SetDestination(chargedFrom);
        if (Vector3.Distance(myTransform.position,chargedFrom) <= 1)
        {
            b_BackOff = false;
            startedCharging = false;
        }
            

    }

    void Charge()
    {
        ChargeTarget();
        myNavFollow.chasing = false;
        myNavAgent.SetDestination(chargeTarget);
        myNavAgent.speed = chargeSpeed;
        myNavAgent.stoppingDistance = 0;
        myNavAgent.acceleration = chargeAcceleration;
        myAnimator.Play("ChildAttack");

        //deal damage
        if(!hasHit)
        {
            
            if(Vector3.Distance(myTransform.position, p_Transform.position) <= 2)
            {
                p_Transform.GetComponent<PlayerTakeDamage>().playerHealth -= pAttackDamage;
                hasHit = true;
            }
        }
        
        if(Vector3.Distance(myTransform.position, chargeTarget) <= 0.5f)
        {
            myNavAgent.acceleration = normalAcceleration;
            myNavAgent.speed = normalSpeed;
            if(waitTimerBool)
            {
                waitTimer = 1;
                waitTimerBool = false;
            }
            //wait a sec
            if(waitTimer <= 0)
            {
                targetChosen = false;
                b_PrimaryAttack = false;
                b_BackOff = true;
                pAttacKCD = 10;
            }
            
        }
        else if(Vector3.Angle(myTransform.forward, chargeTarget - myTransform.position) > 100)
        {
            myNavAgent.acceleration = normalAcceleration;
            myNavAgent.speed = normalSpeed;
            if (waitTimerBool)
            {
                waitTimer = 1;
                waitTimerBool = false;
            }
            //wait a sec
            if (waitTimer <= 0)
            {
                targetChosen = false;
                b_PrimaryAttack = false;
                b_BackOff = true;
                pAttacKCD = 10;
            }
        }
        
    }

    void Chase()
    {
        if (myNavAgent.speed != normalSpeed)
            myNavAgent.speed = normalSpeed;
        if (myNavAgent.acceleration != normalAcceleration)
            myNavAgent.acceleration = normalAcceleration;
        if (!myNavFollow.chasing)
            myNavFollow.chasing = true;
        if (targetChosen)
            targetChosen = false;
        

        myAnimator.Play("ChildRun");
    }

    void ChargeTarget()
    {
        if(!targetChosen)
        {
            chargedFrom = myTransform.position;
            chargeTarget = p_Transform.position;
            targetChosen = true;
        }
    }
}
