﻿using UnityEngine;
using System.Collections;

public class PlayerTakeDamage : MonoBehaviour {


    public int playerHealth;

	// Use this for initialization
	void Start () {
        playerHealth = 10;
	
	}
	
	// Update is called once per frame
	void Update () {
        if(playerHealth <= 0)
        {
            Debug.Log("Dead");
        }
	
	}

    void takeDamage(int damage)
    {
        playerHealth -= damage;
    }
}
