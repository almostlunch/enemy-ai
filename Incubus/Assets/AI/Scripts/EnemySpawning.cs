﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawning : MonoBehaviour {
    private int numberOfEnemies;
    private int EnemyIndex;
    private int EnemiesSpawned = 0;

    List<int> EnemyIndices;

    public GameObject Imp;
    public GameObject Cyclops;
    public GameObject Torcher;

    List<GameObject> Imps;
    List<GameObject> Cyclopses;
    List<GameObject> Torchers;

    bool spawnedOne = false;

    private int counter = 0;


    // Use this for initialization
    void Start()
    {

       /* Imps = GameObject.FindGameObjectWithTag("ScriptManager").GetComponent<DemonLists>().Imps;
        Cyclopses = GameObject.FindGameObjectWithTag("ScriptManager").GetComponent<DemonLists>().Cyclopses;
        Torchers = GameObject.FindGameObjectWithTag("ScriptManager").GetComponent<DemonLists>().Torchers;
        EnemyIndices = new List<int>();
        numberOfEnemies = Random.Range(3, 9);

        for (int i = 0; i < numberOfEnemies; i++)
        {
            EnemyIndex = Random.Range(1, 4);
            EnemyIndices.Add(EnemyIndex);
        }*/

    }

    public void spawnEnemy()
    {
        
        Instantiate(Imp, transform.position, transform.rotation);
        Instantiate(Cyclops, transform.position, transform.rotation);
        Instantiate(Torcher, transform.position, transform.rotation);

        counter++;
        
        if(counter > 3)
        {
            Destroy(transform.parent.gameObject);
        }

        /* spawnedOne = false;
         if (!spawnedOne)
         {
             spawnedOne = true;
             if (EnemiesSpawned < numberOfEnemies)
             {
                 if (EnemyIndices[EnemiesSpawned] == 1)
                 {
                     for (int i = 0; i < Imps.Count; i++)
                     {
                         if (!Imps[i].activeInHierarchy)
                         {
                             Imps[i].transform.position = transform.position;
                             Imps[i].transform.rotation = Quaternion.identity;
                             Imps[i].SetActive(true);
                             EnemiesSpawned++;
                             break;
                         }

                     }
                 }

                 if (EnemyIndices[EnemiesSpawned] == 2)
                 {
                     for (int i = 0; i < Cyclopses.Count; i++)
                     {
                         if (!Imps[i].activeInHierarchy)
                         {
                             Cyclopses[i].transform.position = transform.position;
                             Cyclopses[i].transform.rotation = Quaternion.identity;
                             Cyclopses[i].SetActive(true);
                             EnemiesSpawned++;
                             break;
                         }

                     }
                 }

                 if (EnemyIndices[EnemiesSpawned] == 3)
                 {
                     for (int i = 0; i < Torchers.Count; i++)
                     {
                         if (!Imps[i].activeInHierarchy)
                         {
                             Torchers[i].transform.position = transform.position;
                             Torchers[i].transform.rotation = Quaternion.identity;
                             Torchers[i].SetActive(true);
                             EnemiesSpawned++;
                             break;
                         }

                     }
                 }
             }
             else
             {
                 Destroy(transform.parent.gameObject);
             }
         }*/
    }


}