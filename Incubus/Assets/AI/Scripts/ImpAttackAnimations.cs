﻿using UnityEngine;
using System.Collections;

public class ImpAttackAnimations : MonoBehaviour {

    private Transform myTransform;
    private Transform p_Transform;
    private NavMeshAgent myNavMesh;
    private Transform positionToShoot;

    bool attackFinished = false;
    private Animator myAnimator;
    public float distanceToAttack = 10;
    public float cooldown;

    private bool cooldownFinished;

    // Use this for initialization
    void Start () {

        myTransform = transform;
        p_Transform = GameObject.FindGameObjectWithTag("Player").transform;
        myAnimator = transform.GetComponent<Animator>();
        myNavMesh = transform.parent.transform.parent.transform.GetComponent<NavMeshAgent>();
        positionToShoot = transform.FindChild("shoot").transform;
    }
	
	// Update is called once per frame
	void Update () {

        if(cooldown<=0)
        {
            cooldownFinished = true;
        }
        else
        {
            cooldown -= Time.deltaTime;
        }
        positionToShoot.LookAt(new Vector3(p_Transform.position.x, p_Transform.position.y, p_Transform.position.z));

        if (Vector3.Distance(myTransform.position, p_Transform.position) < distanceToAttack && cooldownFinished)
        {            
            ShootFireball();
            cooldown = 10;
            cooldownFinished = false;
        }
        else
        {
            ChasePlayer();
        }

    }

    void ShootFireball()
    {
        attackFinished = false;
        myAnimator.Play("ImpAttack");

    }

    void ChasePlayer()
    {
        if(attackFinished)
        {
            myAnimator.Play("ImpRun");
        }

            
    }

    void AttackFinished()
    {
        attackFinished = true;
    }

}
