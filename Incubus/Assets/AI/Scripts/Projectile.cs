﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public float force = 100.0f;

    public string otherName;
    public string otherotherName;
    private Rigidbody m_rigidbody;
    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_rigidbody.velocity = (transform.forward * force);
        StartCoroutine("Destroy");

    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
       /* if (other.name == "Player")
            GameObject.FindGameObjectWithTag("Player")*/
    }

}
