﻿using UnityEngine;
using System.Collections;

public class SpawnPortal : MonoBehaviour {

    bool spawned = true;
    public static bool canSpawn = false;

    //prevernt portal from spawning for 30 seconds
    private float timer = 0.0f;

    //room settings
    public GameObject[] portals;
    private int selected = 0;

    void Update()
    {
        if(timer < 30)
            timer += Time.deltaTime;
        else if(!canSpawn)
        {
            Spawn();
        }

    }
    void OnTriggerEnter(Collider col)
    {
        if (canSpawn)
        {
            Debug.Log("Here");
            if (col.tag == "Player")
            {

                if (spawned)
                {
                    spawned = false;
                    float chance = Random.Range(0, 100);
                    if (chance >= 80)
                    {
                        if (this.gameObject.tag == "Type1")
                        {
                            selected = 0;
                            Instantiate(portals[selected], new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), Quaternion.identity);
                        }
                         else if (this.gameObject.tag == "Type2")
                        {
                            selected = 1;
                            Instantiate(portals[selected], new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), Quaternion.identity);
                        }
                        else if (this.gameObject.tag == "Type3")
                        {
                            selected = 2;
                            Instantiate(portals[selected], new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), Quaternion.identity);
                        }
                        else if (this.gameObject.tag == "Type4")
                        {
                            selected = 3;
                            Instantiate(portals[selected], new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), Quaternion.identity);
                        }
                    }
                }
            }
        }
    }

    public void Spawn() {
        canSpawn = true;
    }

}
