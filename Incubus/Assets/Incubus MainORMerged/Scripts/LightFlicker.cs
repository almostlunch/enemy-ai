﻿using UnityEngine;
using System.Collections;

public class LightFlicker : MonoBehaviour {


    public Light flickerL;

    // Update is called once per frame
    void Update()
    {
        if (Random.value > 0.9)
        {
            if (flickerL.enabled == true)
            {
                flickerL.enabled = false;
            }
            else
            {
                flickerL.enabled = true;
            }
        }
    }

}
