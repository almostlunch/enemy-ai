﻿using UnityEngine;
using System.Collections;

public class BossRoom : MonoBehaviour {

    public Vector3 bossRoomMinSize, bossRoomMaxSize;
    private Vector3 bossRoomSize;

    void Awake()
    {
        bossRoomSize.x = Random.Range(bossRoomMinSize.x, bossRoomMaxSize.x);
        bossRoomSize.y = Random.Range(bossRoomMinSize.y, bossRoomMaxSize.y);
        bossRoomSize.z = Random.Range(bossRoomMinSize.z, bossRoomMaxSize.z);
        transform.localScale = bossRoomSize;
    }
}
