﻿using UnityEngine;
using System.Collections;

public class DestroyParticleExplosion : MonoBehaviour
{
    private float timer = 1.0f;

    void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0.0f)
        {
            Destroy(gameObject);
        }
    }
}