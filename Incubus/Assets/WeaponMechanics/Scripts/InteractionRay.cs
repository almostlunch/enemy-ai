﻿using UnityEngine;
using System.Collections;

public class InteractionRay : MonoBehaviour
{
    private GameObject m_camera;
    RaycastHit[] hits;
    RaycastHit hit;

    void Start()
    {
        m_camera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    void Update()
    {
        Vector3 dir = m_camera.transform.rotation * Vector3.forward * 5.0f;

        hits = Physics.RaycastAll(m_camera.transform.position, dir, 10.0f);
        Debug.DrawRay(m_camera.transform.position, dir, Color.red, 10.0f);

        if (Physics.Raycast(m_camera.transform.position, dir, out hit))
        {
            if (hit.collider.gameObject.tag == "Interactable")
            {
                //Get info to display to the character
            }
        }
    }
}