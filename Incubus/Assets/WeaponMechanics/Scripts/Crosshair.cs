﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour
{
    public bool up;
    public bool down;
    public bool left;
    public bool right;

    private Vector3 startPos;
    private Vector3 endPos;
    private Vector3 varPos;

    private static float lerpNo;
    private static float multiplier;

    void Start()
    {
        startPos = transform.position;

        
    }

    void Update()
    {
        if (up)
        {
            varPos = new Vector3(0.0f, 0.5f * (multiplier * 100), 0.0f);
        }
        else if (down)
        {
            varPos = new Vector3(0.0f, -0.5f * (multiplier * 100), 0.0f);
        }
        else if (left)
        {
            varPos = new Vector3(-0.5f * (multiplier * 100), 0.0f, 0.0f);
        }
        else if (right)
        {
            varPos = new Vector3(0.5f * (multiplier * 100), 0.0f, 0.0f);
        }

        endPos = startPos + varPos;

        transform.position = Vector3.Lerp(startPos, endPos, lerpNo);
    }

    public static void SetLerpNo(float no)
    {
        lerpNo = no;
    }

    public static void SetMultiplier(float multi)
    {
        multiplier = multi;
    }
}