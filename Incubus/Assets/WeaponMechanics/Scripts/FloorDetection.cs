﻿using UnityEngine;
using System.Collections;

public class FloorDetection : MonoBehaviour
{
    private GameObject m_character;

    void Start()
    {
        m_character = GameObject.FindGameObjectWithTag("Player");
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject != null)
        {
            m_character.GetComponent<PlayerController>().SetIsGrounded(true);
            //m_character.GetComponent<JetpackCharge>().RechargingBool();
        }
    }
}