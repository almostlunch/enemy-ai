﻿using UnityEngine;
using System.Collections;

public class Rifle : Gun
{
    new void Start()
    {
        base.Start();

        //maxEnergy = 100;
        //currEnergy = 100;
        energyUse = 1;
        fireRate = 0.1f;
        damage = 5;
        inaccuracy = 0.04f;
        inaccuracyLerp = 0.0f;
        inaccuracyMultiplier = 0.75f;
        noOfBulletsPerShot = 1;

        canShoot = true;

        skillTree = new Pair<string, int>[9];

        PopulateSkillTree();
    }

    new void Update()
    {
        base.Update();

        maxEnergy = WeaponEnergy.GetMaxEnergyRifle();
        currEnergy = WeaponEnergy.GetCurrEnergyRifle();
    }

    public override void AddEnergy(int e)
    {
        WeaponEnergy.AddEnergyRifle(e);
    }

    public void PopulateSkillTree()
    {
        skillTree[0] = new Pair<string, int>("ProcBleed", 0);
        skillTree[1] = new Pair<string, int>("ChanceToNotUseEnergy", 0);
        skillTree[2] = new Pair<string, int>("Damage", 0);

        //LMG
        skillTree[3] = new Pair<string, int>("Accuracy", 0);
        skillTree[4] = new Pair<string, int>("Firerate", 0);
        skillTree[5] = new Pair<string, int>("MaxEnergy", 0);

        //DMR
        skillTree[6] = new Pair<string, int>("BulletPenetration", 0);
        skillTree[7] = new Pair<string, int>("Active - BulletTime", 0);
        skillTree[8] = new Pair<string, int>("MoreDamage", 0);
    }
}