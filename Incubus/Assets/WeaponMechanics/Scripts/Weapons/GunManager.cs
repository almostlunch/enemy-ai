﻿using UnityEngine;
using System.Collections;

public class GunManager : MonoBehaviour
{
    private GameObject rifle;
    private GameObject pistol;
    private GameObject shotgun;

    private int activeWeapon;

    private bool canChange;

    void Start()
    {
        rifle = gameObject.GetComponent<GunGO>().GetRifle();
        shotgun = gameObject.GetComponent<GunGO>().GetShotgun();
        pistol = gameObject.GetComponent<GunGO>().GetPistol();

        pistol.SetActive(false);
        shotgun.SetActive(false);

        activeWeapon = 1;

        canChange = true;
    }

    public void ActivateRifle()
    {
        rifle.SetActive(true);
        pistol.SetActive(false);
        shotgun.SetActive(false);

        activeWeapon = 1;

        canChange = false;
    }

    public void ActivateShotgun()
    {
        rifle.SetActive(false);
        pistol.SetActive(false);
        shotgun.SetActive(true);

        activeWeapon = 2;

        canChange = false;
    }

    public void ActivatePistol()
    {
        rifle.SetActive(false);
        pistol.SetActive(true);
        shotgun.SetActive(false);

        activeWeapon = 3;

        canChange = false;
    }

    public int GetActiveWeapon()
    {
        return activeWeapon;
    }

    public void SetCanChange(bool change)
    {
        canChange = change;
    }

    public bool GetCanChange()
    {
        return canChange;
    }
}