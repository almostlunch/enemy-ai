﻿using UnityEngine;
using System.Collections;

public class Grenade : MonoBehaviour
{
    public GameObject explosionParticle;

    private float speed;
    private float explosionTimer;
    private float explosionPower;
    private float explosionRadius;

    void Start()
    {
        speed = 5.0f;
        explosionTimer = 3.0f;
        explosionPower = 1000.0f;
        explosionRadius = 10.0f;
        this.GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.Impulse);
    }

    void Update()
    {
        explosionTimer -= Time.deltaTime;

        if (explosionTimer <= 0.0f)
        {
            Explode();
        }
    }

    void Explode()
    {
        Instantiate(explosionParticle, transform.position, transform.rotation);

        Collider[] objectsInRange = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider hit in objectsInRange)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
            {
                rb.AddExplosionForce(explosionPower, transform.position, explosionRadius);
            }
        }

        Destroy(gameObject);
    }
}