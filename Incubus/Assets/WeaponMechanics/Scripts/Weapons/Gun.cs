﻿using UnityEngine;
using System.Collections;
using System.Linq;

public abstract class Gun : MonoBehaviour
{
    public GameObject m_player;
    public GameObject m_camera;
    public GameObject scriptManager;
    public GameObject m_trail;
    public GameObject bulletLeave;
    public GameObject bulletImpact;
    public GameObject muzzleFlash;
    public GameObject parentGun;
    public GameObject parentGunRot;
    public GameObject theCamera;
    public GameObject smoke;
    public GameObject[] movingParts;

    public bool canShoot;
    private float shootTimer;

    protected int maxEnergy;
    protected int currEnergy;
    protected int energyUse;
    protected float fireRate;
    protected int damage;
    protected float inaccuracy;
    protected float inaccuracyLerp;
    protected float inaccuracyMultiplier;
    protected float noOfBulletsPerShot;

    protected float timeGap;

    //Weapon levels
    protected int level;
    protected int exp;
    protected int[] expBracket;
    protected int skillPoints;

    //Weapon skills
    protected Pair<string, int>[] skillTree;

    //Confusing math stuff for bullet trail angles
    protected Vector3 cameraRightAngle;
    protected float offsetTrailAngle;

    public void Start()
    {
        //m_player = GameObject.FindGameObjectWithTag("Player");
        //m_camera = GameObject.FindGameObjectWithTag("MainCamera");
        shootTimer = 0.0f;

        timeGap = 0.03f;

        level = 0;
        exp = 0;
        skillPoints = 0;
        expBracket = new int[30];
        PopulateExpBracket();
    }

    public void Update()
    {
        if (!canShoot)
        {
            shootTimer += Time.deltaTime;

            if (shootTimer >= fireRate)
            {
                canShoot = true;
                shootTimer = 0.0f;
            }
        }

        //Inaccuracy extra stuff
        if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() != 2)
        {
            inaccuracyLerp -= timeGap;

            if (inaccuracyLerp < 0.0f)
            {
                inaccuracyLerp = 0.0f;
            }
        }

        //Crosshair manipulation
        Crosshair.SetLerpNo(inaccuracyLerp);
        Crosshair.SetMultiplier(inaccuracyMultiplier);
    }

    public void Shoot()
    {
        if (canShoot)
        {
            if (currEnergy > 0)
            {
                //currEnergy -= energyUse;
                AddEnergy(-energyUse);
                canShoot = false;
                for (int i = 0; i < noOfBulletsPerShot; i++)
                {
                    Vector3 dir = m_camera.transform.rotation * Vector3.forward;
                    Vector3 inaccuracyVector = new Vector3(
                        Random.Range(-inaccuracy * inaccuracyLerp, inaccuracy * inaccuracyLerp), 
                        Random.Range(-inaccuracy * inaccuracyLerp, inaccuracy * inaccuracyLerp), 
                        Random.Range(-inaccuracy * inaccuracyLerp, inaccuracy * inaccuracyLerp));

                    RaycastHit[] hits;

                    hits = Physics.RaycastAll(m_camera.transform.position, dir + inaccuracyVector, 1000.0f).OrderBy(h=>h.distance).ToArray();

                    //If something it hit then tell the bullet trail to move towards the end point of the hit else give it an angle offset to make it look accurate
                    if (hits.Length != 0)
                    {
                        offsetTrailAngle = Angles(hits[0].point);

                        for (int j = 0; j < 3; j++)
                        {
                            Debug.Log(hits[j].transform.gameObject.tag + " & " + hits[j].transform.gameObject.name);
                        }

                        //Do stuff depending on what it hit
                        if (hits[0].collider.gameObject.tag == "Damageable")
                        {
                            hits[0].transform.gameObject.GetComponent<Damageable>().TakeDamage(damage);
                        }
                        else if (hits[0].collider.gameObject.tag == "Wall" || hits[0].collider.gameObject.tag == "Floor")
                        {
                            Instantiate(bulletImpact, hits[0].point + (hits[0].normal * 0.001f), Quaternion.FromToRotation(Vector3.up, hits[0].normal));
                        }

                        GameObject trail = (GameObject)Instantiate(m_trail, bulletLeave.transform.position, Quaternion.LookRotation(dir + inaccuracyVector));
                        trail.GetComponent<TrailForce>().SetEndPoint(hits[0].point);

                        //Create small impact explosion
                        Collider[] objsInRange = Physics.OverlapSphere(hits[0].point, 2.0f);

                        foreach (Collider col in objsInRange)
                        {
                            Rigidbody rb = col.GetComponent<Rigidbody>();

                            if (rb != null)
                            {
                                rb.AddExplosionForce(100.0f, hits[0].point, 5.0f);
                            }
                        }
                    }
                    else
                    {
                        offsetTrailAngle = 1.0f;
                        Instantiate(m_trail, bulletLeave.transform.position, Quaternion.LookRotation(dir + inaccuracyVector) * Quaternion.Euler(0, -offsetTrailAngle, 0));
                    }
                }

                //Muzzle flash
                GameObject mFlash = (GameObject)Instantiate(muzzleFlash, bulletLeave.transform.position, bulletLeave.transform.rotation * Quaternion.Euler(0, 0, Random.Range(0, 360)));
                mFlash.GetComponent<FollowBulletLeave>().SetBulletLeave(bulletLeave);

                //Smoke
                Instantiate(smoke, bulletLeave.transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f));

                canShoot = false;

                //Stuff for other scripts to make it more "gun-like"
                //parentGun.GetComponent<WeaponMovement>().ShotFalse();
                parentGun.GetComponent<WeaponMovement>().ShotTrue();

                if (parentGunRot != null)
                {
                    parentGunRot.GetComponent<WeaponRotate>().ShotTrue();
                }

                foreach (GameObject go in movingParts)
                {
                    //go.GetComponent<MovingParts>().ShotFalse();
                    go.GetComponent<MovingParts>().ShotTrue();
                }

                theCamera.GetComponent<CameraRecoil>().ShotTrue();

                //Setup inaccuracy for spraying
                if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() != 2)
                {
                    inaccuracyLerp += timeGap * 10 * inaccuracyMultiplier;

                    if (inaccuracyLerp > 1.0f)
                    {
                        inaccuracyLerp = 1.0f;
                    }
                }
            }
        }
    }

    protected void PopulateExpBracket()
    {
        int baseExpReq = 200;

        for (int i = 0; i < 30; i++)
        {
            expBracket[i] = baseExpReq + (baseExpReq / 2) * i;
        }
    }

    public void GainExp(int no)
    {
        exp += no;

        CheckLevelUp();
    }

    public void CheckLevelUp()
    {
        if (exp >= expBracket[level])
        {
            int leftover;
            leftover = exp - expBracket[level];
            exp = leftover;

            level++;
            skillPoints++;
        }
    }

    //Obselete but I can't bring myself to delete this
    public float Angles(Vector3 hitPoint)
    {
        float distOpposite;
        float distAdjacent;

        cameraRightAngle = m_camera.transform.position;
        cameraRightAngle.z = bulletLeave.transform.position.z;
        cameraRightAngle.x = bulletLeave.transform.position.x;

        distAdjacent = Vector3.Distance(cameraRightAngle, hitPoint);
        distOpposite = Vector3.Distance(cameraRightAngle, bulletLeave.transform.position);

        float result = Mathf.Rad2Deg * Mathf.Atan2(distOpposite, distAdjacent);

        return result;
    }

    public void SetMaxEnergy(int energy)
    {
        maxEnergy = energy;
    }

    public void SetCurrEnergy(int energy)
    {
        currEnergy = energy;
    }

    public int GetMaxEnergy()
    {
        return maxEnergy;
    }

    public int GetCurrEnergy()
    {
        return currEnergy;
    }

    public abstract void AddEnergy(int e);
}