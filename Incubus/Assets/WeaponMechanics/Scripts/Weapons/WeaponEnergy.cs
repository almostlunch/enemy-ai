﻿using UnityEngine;
using System.Collections;

public class WeaponEnergy : MonoBehaviour
{
    private static int rifleMaxEnergy;
    private static int rifleCurrEnergy;
    private static int shotgunMaxEnergy;
    private static int shotgunCurrEnergy;

    private const float RIFLE_CHARGE_TIMER = 0.5f;
    private const float SHOTGUN_CHARGE_TIMER = 1.5f;

    private float rifleChargeTimer;
    private float shotgunChargeTimer;

    void Start()
    {
        rifleMaxEnergy = 100;
        rifleCurrEnergy = 100;
        shotgunMaxEnergy = 100;
        shotgunCurrEnergy = 100;

        rifleChargeTimer = RIFLE_CHARGE_TIMER;
        shotgunChargeTimer = SHOTGUN_CHARGE_TIMER;
    }

    void Update()
    {
        if (rifleCurrEnergy < rifleMaxEnergy)
        {
            rifleChargeTimer -= Time.deltaTime;

            if (rifleChargeTimer <= 0.0f)
            {
                rifleCurrEnergy += 1;
                rifleChargeTimer = RIFLE_CHARGE_TIMER;
            }
        }

        if (shotgunCurrEnergy < shotgunMaxEnergy)
        {
            shotgunChargeTimer -= Time.deltaTime;

            if (shotgunChargeTimer <= 0.0f)
            {
                shotgunCurrEnergy += 1;
                shotgunChargeTimer = SHOTGUN_CHARGE_TIMER;
            }
        }
    }

    public static void SetMaxEnergyRifle(int energy)
    {
        rifleMaxEnergy = energy;
    }

    public static void SetCurrEnergyRifle(int energy)
    {
        rifleCurrEnergy = energy;
    }

    public static int GetMaxEnergyRifle()
    {
        return rifleMaxEnergy;
    }

    public static int GetCurrEnergyRifle()
    {
        return rifleCurrEnergy;
    }

    public static void SetMaxEnergyShotgun(int energy)
    {
        shotgunMaxEnergy = energy;
    }

    public static void SetCurrEnergyShotgun(int energy)
    {
        shotgunCurrEnergy = energy;
    }

    public static int GetMaxEnergyShotgun()
    {
        return shotgunMaxEnergy;
    }

    public static int GetCurrEnergyShotgun()
    {
        return shotgunCurrEnergy;
    }

    public static void AddEnergyRifle(int e)
    {
        rifleCurrEnergy += e;

        if (rifleCurrEnergy > rifleMaxEnergy)
        {
            rifleCurrEnergy = rifleMaxEnergy;
        }
    }

    public static void AddEnergyShotgun(int e)
    {
        shotgunCurrEnergy += e;

        if (shotgunCurrEnergy > shotgunMaxEnergy)
        {
            shotgunCurrEnergy = shotgunMaxEnergy;
        }
    }
}