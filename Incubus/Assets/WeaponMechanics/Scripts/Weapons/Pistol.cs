﻿using UnityEngine;
using System.Collections;

public class Pistol : Gun
{
    new void Start()
    {
        base.Start();

        maxEnergy = 999;
        currEnergy = 999;
        energyUse = 0;
        fireRate = 0.2f;
        damage = 5;
        inaccuracy = 0.02f;
        inaccuracyLerp = 0.0f;
        inaccuracyMultiplier = 1.5f;
        noOfBulletsPerShot = 1;

        canShoot = true;

        skillTree = new Pair<string, int>[6];

        PopulateSkillTree();
    }

    public override void AddEnergy(int e)
    {
        
    }

    public void PopulateSkillTree()
    {
        skillTree[0] = new Pair<string, int>("Firerate", 0);
        skillTree[1] = new Pair<string, int>("Damage", 0);
        skillTree[2] = new Pair<string, int>("ChanceProcLifesteal", 0);
        skillTree[3] = new Pair<string, int>("ChanceProcMoveSpeedBuff", 0);
        skillTree[4] = new Pair<string, int>("ChanceProcInstaKill", 0);
        skillTree[5] = new Pair<string, int>("ChanceProcMultiShot", 0);

        //Augment: Double skill points for one skill (skill cap for that skill is also doubled)
    }
}