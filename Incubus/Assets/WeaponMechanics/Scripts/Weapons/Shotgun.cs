﻿using UnityEngine;
using System.Collections;

public class Shotgun : Gun
{
    new void Start()
    {
        base.Start();

        //maxEnergy = 100;
        //currEnergy = 100;
        energyUse = 5;
        fireRate = 0.75f;
        damage = 5;
        inaccuracy = 0.07f;
        inaccuracyLerp = 1.0f;
        inaccuracyMultiplier = 1.0f;
        noOfBulletsPerShot = 10;

        canShoot = true;

        skillTree = new Pair<string, int>[9];

        PopulateSkillTree();
    }

    new void Update()
    {
        base.Update();

        maxEnergy = WeaponEnergy.GetMaxEnergyShotgun();
        currEnergy = WeaponEnergy.GetCurrEnergyShotgun();
    }

    public override void AddEnergy(int e)
    {
        WeaponEnergy.AddEnergyShotgun(e);
    }

    public void PopulateSkillTree()
    {
        skillTree[0] = new Pair<string, int>("Damage", 0);
        skillTree[1] = new Pair<string, int>("ChanceToExplodeOnImpact", 0);
        skillTree[2] = new Pair<string, int>("Firerate", 0);

        //Slug round
        skillTree[3] = new Pair<string, int>("BiggerExplosionForSlug", 0);
        skillTree[4] = new Pair<string, int>("IgniteGroundOnExplosion", 0);
        skillTree[5] = new Pair<string, int>("", 0);

        //Dual-wield
        skillTree[6] = new Pair<string, int>("DragonBreath", 0);
        skillTree[7] = new Pair<string, int>("Spread", 0);
        skillTree[8] = new Pair<string, int>("", 0);
    }
}