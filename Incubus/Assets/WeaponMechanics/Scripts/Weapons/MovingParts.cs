﻿using UnityEngine;
using System.Collections;

public class MovingParts : MonoBehaviour
{
    public GameObject scriptManager;

    public bool isLeftSide;
    private bool shot;
    private float lerpNo;
    private float recoilMultiplier;
    private float recoilBackMultiplier;
    private Vector3 startPos;
    private Vector3 endPos;
    private Vector3 difPos;

    void Start()
    {
        startPos = transform.localPosition;
    }

    void Update()
    {
        if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 1)
        {
            if (isLeftSide)
            {
                difPos = new Vector3(0.5f, 0.0f, 0.0f);
            }
            else
            {

            }

            recoilMultiplier = 10.0f;
            recoilBackMultiplier = 10.0f;
        }
        else if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 2)
        {
            if (isLeftSide)
            {

            }
            else
            {
                difPos = new Vector3(0.5f, 0.0f, 0.0f);
            }

            recoilMultiplier = 5.0f;
            recoilBackMultiplier = 100.0f;
        }
        else if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() == 3)
        {
            if (isLeftSide)
            {
                difPos = new Vector3(4.0f, 0.0f, 0.0f);
            }
            else
            {
                difPos = new Vector3(-4.0f, 0.0f, 0.0f);
            }

            recoilMultiplier = 1.0f;
            recoilBackMultiplier = 1.0f;
        }

        endPos = startPos + difPos;

        PartMove();
    }

    void PartMove()
    {
        if (shot)
        {
            lerpNo += Time.deltaTime * 20 * recoilMultiplier;

            if (lerpNo > 1.0f)
            {
                lerpNo = 1.0f;
            }
        }
        else
        {

            if (lerpNo < 0.0f)
            {
                lerpNo = 0.0f;
            }
            else
            {
                lerpNo -= Time.deltaTime * recoilBackMultiplier;
            }
        }

        shot = false;

        transform.localPosition = Vector3.Lerp(startPos, endPos, lerpNo);
    }

    public void ShotTrue()
    {
        shot = true;
    }

    public void ShotFalse()
    {
        shot = false;
    }
}