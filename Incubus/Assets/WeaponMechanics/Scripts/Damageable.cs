﻿using UnityEngine;
using System.Collections;

public class Damageable : MonoBehaviour
{
    public int health = 10;
    private int maxHealth;
    public GameObject ragDoll;


    void Start()
    {
        health = 10;
    }



    void OnEnable()
    {
        health = maxHealth;
    }


    public void TakeDamage(int dmg)
    {
        health -= dmg;
        Debug.Log(gameObject.name + health);

        if (health <= 0)
        {
            ragDoll.transform.position = transform.position;
            ragDoll.transform.rotation = transform.rotation;
            ragDoll.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}