﻿using UnityEngine;
using System.Collections;

public class LaserHit : MonoBehaviour {
    private bool damageDelay = true;
    private float delayTimer = 0;

    void Update()
    {
        if (delayTimer <= 0 && !damageDelay)
            damageDelay = true;
        else if(delayTimer > 0)
        {
            delayTimer -= Time.deltaTime;
        }
        
    }
	void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && damageDelay)
        {
            Debug.Log("Player Takes Damage");
            damageDelay = false;
            delayTimer = 0.2f;
        }
    }
}
