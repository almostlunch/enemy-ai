﻿using UnityEngine;
using System.Collections;

public class JetpackCharge : MonoBehaviour
{
    private GameObject m_character;

    private float jetpackCharge;
    private bool canRecharge;

    private const float RECHARGE_DELAY = 0.5f;
    private float rechargeTimer = RECHARGE_DELAY;

    void Start()
    {
        m_character = GameObject.FindGameObjectWithTag("Player");
        jetpackCharge = 100.0f;
        canRecharge = false;
    }

    void Update()
    {
        Recharging();
        OverchargeCheck();
        CanRecharge();
    }

    public void UseJetpack()
    {
        jetpackCharge -= 2.0f;
        rechargeTimer = RECHARGE_DELAY;

        if (canRecharge)
            canRecharge = false;
    }

    public void RechargingBool()
    {
        canRecharge = true;
    }

    private void Recharging()
    {
        if (canRecharge)
            jetpackCharge += 1.0f;
    }

    private void OverchargeCheck()
    {
        if (jetpackCharge >= 100.0f)
        {
            jetpackCharge = 100.0f;
            canRecharge = false;
        }
    }

    private void CanRecharge()
    {
        rechargeTimer -= Time.deltaTime;

        if (rechargeTimer <= 0.0f)
        {
            rechargeTimer = 0.0f;
            canRecharge = true;
        }
    }

    public float GetJetpackCharge()
    {
        return jetpackCharge;
    }
}