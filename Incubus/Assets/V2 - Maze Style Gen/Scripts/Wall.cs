﻿using UnityEngine;
using System.Collections;

public class Wall : LevelCellEdge {
    public Transform wall;

    public override void InitializeEdges (LevelCell cell, LevelCell otherCell, GenerationDirection direction)
    {
        base.InitializeEdges(cell, otherCell, direction);
        wall.GetComponent<Renderer>().material = cell.room.settings.wallMat;
    }
}
