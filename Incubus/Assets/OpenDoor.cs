﻿using UnityEngine;
using System.Collections;

public class OpenDoor : MonoBehaviour {

    public Animator doorCTRL;

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            doorCTRL.Play("DoorRise");
        }
    }
}
